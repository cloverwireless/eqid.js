module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-jasmine-node');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-karma');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            all: [
                'Gruntfile.js',
                'lib/**/*.js',
                'test/spec/**/*.js'
            ]
        },

        karma: {
            unit: {
                configFile: 'karma.conf.js'
            }
        },

        jasmine_node: {
            specNameMatcher: 'spec',
            projectRoot: '.'
        },

        uglify: {
            all: {
                files: {
                    'eqid.min.js': [
                        'lib/eqid.js'
                    ]
                }
            }
        }

    });

    grunt.registerTask('default', ['jshint', 'jasmine_node']);
};

