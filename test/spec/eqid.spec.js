(function() {
    var eqid = this.eqid;
    if (!eqid) {
        var eqid = require('../../lib/eqid.js');
    }

    var VECTORS = [
        {
            'input': '99000334708934',
            'identifier': '99000334708934',
            'checksum': '0',
            'full_identifier': '990003347089340',
            'format': eqid.IMEI,
            'is_imei': true,
            'is_meid': true,
            'is_esn': false,
            'is_pseudo_esn': false,
            'is_hex': true,
            'is_dec': false,
            'as_meid_dec': '256691486807375156',
            'as_full_meid_dec': '2566914868073751565',
            'as_meid_hex': '99000334708934',
            'as_full_meid_hex': '990003347089340',
            'as_esn_hex': '801A6C37',
            'as_esn_dec': '12801731639',
            'as_imei': '99000334708934',
            'as_full_imei': '990003347089340',
            'as_pseudo_esn': '801A6C37'
        },
        {
            'input': '990001049874696',
            'identifier': '99000104987469',
            'checksum': '6',
            'full_identifier': '990001049874696',
            'format': eqid.IMEI,
            'is_imei': true,
            'is_meid': true,
            'is_esn': false,
            'is_pseudo_esn': false,
            'is_hex': true,
            'is_dec': false,
            'as_meid_dec': '256691430809991273',
            'as_full_meid_dec': '2566914308099912735',
            'as_meid_hex': '99000104987469',
            'as_full_meid_hex': '990001049874696',
            'as_esn_hex': '80D50FDB',
            'as_esn_dec': '12813963227',
            'as_imei': '99000104987469',
            'as_full_imei': '990001049874696',
            'as_pseudo_esn': '80D50FDB'
        },
        {
            'input': 'A1000017AEDA88',
            'identifier': 'A1000017AEDA88',
            'checksum': '3',
            'full_identifier': 'A1000017AEDA883',
            'format': eqid.MEID_HEX,
            'is_imei': false,
            'is_meid': true,
            'is_esn': false,
            'is_pseudo_esn': false,
            'is_hex': true,
            'is_dec': false,
            'as_meid_dec': '270113179911459208',
            'as_full_meid_dec': '2701131799114592082',
            'as_meid_hex': 'A1000017AEDA88',
            'as_full_meid_hex': 'A1000017AEDA883',
            'as_esn_hex': '805FAFE6',
            'as_esn_dec': '12806270950',
            'as_imei': null,
            'as_full_imei': null,
            'as_pseudo_esn': '805FAFE6'
        },
        {
            'input': 'A000000EE97CA0E',
            'identifier': 'A000000EE97CA0',
            'checksum': 'E',
            'full_identifier': 'A000000EE97CA0E',
            'format': eqid.MEID_HEX,
            'is_imei': false,
            'is_meid': true,
            'is_esn': false,
            'is_pseudo_esn': false,
            'is_hex': true,
            'is_dec': false,
            'as_meid_dec': '268435457415301792',
            'as_full_meid_dec': '2684354574153017921',
            'as_meid_hex': 'A000000EE97CA0',
            'as_full_meid_hex': 'A000000EE97CA0E',
            'as_esn_hex': '805D84A0',
            'as_esn_dec': '12806128800',
            'as_imei': null,
            'as_full_imei': null,
            'as_pseudo_esn': '805D84A0'
        },
        {
            'input': '270113180310174687',
            'identifier': '270113180310174687',
            'checksum': '3',
            'full_identifier': '2701131803101746873',
            'format': eqid.MEID_DEC,
            'is_imei': false,
            'is_meid': true,
            'is_esn': false,
            'is_pseudo_esn': false,
            'is_hex': false,
            'is_dec': true,
            'as_meid_dec': '270113180310174687',
            'as_full_meid_dec': '2701131803101746873',
            'as_meid_hex': 'A100001B9B40DF',
            'as_full_meid_hex': 'A100001B9B40DFC',
            'as_esn_hex': '80990A73',
            'as_esn_dec': '12810029683',
            'as_imei': null,
            'as_full_imei': null,
            'as_pseudo_esn': '80990A73'
        },
        {
            'input': '2684354608134991067',
            'identifier': '268435460813499106',
            'checksum': '7',
            'full_identifier': '2684354608134991067',
            'format': eqid.MEID_DEC,
            'is_imei': false,
            'is_meid': true,
            'is_esn': false,
            'is_pseudo_esn': false,
            'is_hex': false,
            'is_dec': true,
            'as_meid_dec': '268435460813499106',
            'as_full_meid_dec': '2684354608134991067',
            'as_meid_hex': 'A0000030CDFAE2',
            'as_full_meid_hex': 'A0000030CDFAE26',
            'as_esn_hex': '80E26A52',
            'as_esn_dec': '12814838354',
            'as_imei': null,
            'as_full_imei': null,
            'as_pseudo_esn': '80E26A52'
        },
        {
            'input': '363ED691',
            'identifier': '363ED691',
            'checksum': null,
            'full_identifier': '363ED691',
            'format': eqid.ESN_HEX,
            'is_imei': false,
            'is_meid': false,
            'is_esn': true,
            'is_pseudo_esn': false,
            'is_hex': true,
            'is_dec': false,
            'as_meid_dec': null,
            'as_full_meid_dec': null,
            'as_meid_hex': null,
            'as_full_meid_hex': null,
            'as_esn_hex': '363ED691',
            'as_esn_dec': '05404118161',
            'as_imei': null,
            'as_full_imei': null,
            'as_pseudo_esn': null
        },
        {
            'input': '05401956105',
            'identifier': '05401956105',
            'checksum': null,
            'full_identifier': '05401956105',
            'format': eqid.ESN_DEC,
            'is_imei': false,
            'is_meid': false,
            'is_esn': true,
            'is_pseudo_esn': false,
            'is_hex': false,
            'is_dec': true,
            'as_meid_dec': null,
            'as_full_meid_dec': null,
            'as_meid_hex': null,
            'as_full_meid_hex': null,
            'as_esn_hex': '361DD909',
            'as_esn_dec': '05401956105',
            'as_imei': null,
            'as_full_imei': null,
            'as_pseudo_esn': null
        },
        {
            'input': '80EB6C11',
            'identifier': '80EB6C11',
            'checksum': null,
            'full_identifier': '80EB6C11',
            'format': eqid.PESN_HEX,
            'is_imei': false,
            'is_meid': false,
            'is_esn': true,
            'is_pseudo_esn': true,
            'is_hex': true,
            'is_dec': false,
            'as_meid_dec': null,
            'as_full_meid_dec': null,
            'as_meid_hex': null,
            'as_full_meid_hex': null,
            'as_esn_hex': '80EB6C11',
            'as_esn_dec': '12815428625',
            'as_imei': null,
            'as_full_imei': null,
            'as_pseudo_esn': '80EB6C11'
        },
        {
            'input': 'foobar',
            'identifier': 'FOOBAR',
            'checksum': null,
            'full_identifier': 'FOOBAR',
            'format': eqid.UNKNOWN,
            'is_imei': false,
            'is_meid': false,
            'is_esn': false,
            'is_pseudo_esn': false,
            'is_hex': false,
            'is_dec': false,
            'as_meid_dec': null,
            'as_full_meid_dec': null,
            'as_meid_hex': null,
            'as_full_meid_hex': null,
            'as_esn_hex': null,
            'as_esn_dec': null,
            'as_imei': null,
            'as_full_imei': null,
            'as_pseudo_esn': null
        }
    ];


    for (var i = 0; i < VECTORS.length; i++) {
        /* jshint -W083 */
        describe(VECTORS[i].input, function() {
            var vector = VECTORS[i];
            var e = new eqid.EquipmentID(vector.input);

            it('should be ' + vector.format, function() {
                expect(e.format).toBe(vector.format);
            });

            it('should have identifier of ' + vector.identifier, function() {
                expect(e.identifier).toBe(vector.identifier);
            });

            it('should have checksum of ' + vector.checksum, function() {
                expect(e.checksum).toBe(vector.checksum);
            });

            it('should have the correct flags', function() {
                expect(e.isImei).toBe(vector.is_imei);
                expect(e.isMeid).toBe(vector.is_meid);
                expect(e.isEsn).toBe(vector.is_esn);
                expect(e.isPseudoEsn).toBe(vector.is_pseudo_esn);
                expect(e.isHex).toBe(vector.is_hex);
                expect(e.isDec).toBe(vector.is_dec);
            });

            it('should convert to IMEI appropriately', function() {
                expect(e.asImei()).toBe(vector.as_imei);
                expect(e.asFullImei()).toBe(vector.as_full_imei);
            });

            it('should convert to MEID DEC appropriately', function() {
                expect(e.asMeidDec()).toBe(vector.as_meid_dec);
                expect(e.asFullMeidDec()).toBe(vector.as_full_meid_dec);
            });

            it('should convert to MEID HEX appropriately', function() {
                expect(e.asMeidHex()).toBe(vector.as_meid_hex);
                expect(e.asFullMeidHex()).toBe(vector.as_full_meid_hex);
            });

            it('should convert to ESN HEX appropriately', function() {
                expect(e.asEsnHex()).toBe(vector.as_esn_hex);
            });

            it('should convert to ESN DEC appropriately', function() {
                expect(e.asEsnDec()).toBe(vector.as_esn_dec);
            });

            it('should convert to a Pseudo ESN appropriately', function() {
                expect(e.asPseudoEsn()).toBe(vector.as_pseudo_esn);
            });
        });
    }
}).call(this);

